FROM ubuntu:20.04

MAINTAINER Your Name "youremail@domain.tld"

RUN apt-get update -y && \
    apt-get install -y python3-pip

# We copy just the requirements.txt first to leverage Docker cache
COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip3 install -r requirements.txt

COPY ./flaskr /app/flaskr

ENV FLASK_APP=flaskr
ENV FLASK_ENV=development

# Résoud un problème d'encodage (suggestion du débugger de Flask)
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

RUN flask init-db

# Le --host bind notre serveur sur toutes les ip pour y accéder depuis l'exterieur du docker
# https://devops.stackexchange.com/questions/3380/dockerized-flask-connection-reset-by-peer
CMD [ "flask", "run", "--host=0.0.0.0" ]