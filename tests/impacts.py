"""
Script bête et méchant qui calcule les différents impacts du batiments pour vérifier les résultats de l'API et du calcul SQL.
"""

import json

with open('./flaskr/data/batiments.json') as b_file:
    batiments = json.load(b_file)

with open('./flaskr/data/construction_elements.json') as ce_file:
    construction_elements = json.load(ce_file)

with open('./flaskr/data/usages.json') as u_file:
    usages = json.load(u_file)

with open('./flaskr/data/zones.json') as z_file:
    zones = json.load(z_file)

batimentId = 0

impactProduction = 0 
for batiment in batiments:
    if batiment['id'] == batimentId:
        for zoneId in batiment['zoneIds']:
            for zone in zones:
                if zone['id'] == zoneId:
                    for zoneConstructionElement in zone['constructionElements']:
                        for construction_element in construction_elements:
                            if construction_element['id'] == zoneConstructionElement['id']:
                                impactProduction += construction_element['impactUnitaireRechauffementClimatique']['production'] * zoneConstructionElement['quantite']
print(f'Impact Production : {impactProduction}')

impactConstruction = 0 
for batiment in batiments:
    if batiment['id'] == batimentId:
        for zoneId in batiment['zoneIds']:
            for zone in zones:
                if zone['id'] == zoneId:
                    for zoneConstructionElement in zone['constructionElements']:
                        for construction_element in construction_elements:
                            if construction_element['id'] == zoneConstructionElement['id']:
                                impactConstruction += construction_element['impactUnitaireRechauffementClimatique']['construction'] * zoneConstructionElement['quantite']
print(f'Impact Construction : {impactConstruction}')

impactExploitation = 0
for batiment in batiments:
    if batiment['id'] == batimentId:
        for zoneId in batiment['zoneIds']:
            for zone in zones:
                if zone['id'] == zoneId:
                    for zoneConstructionElement in zone['constructionElements']:
                        for construction_element in construction_elements:
                            if construction_element['id'] == zoneConstructionElement['id']:
                                refSurDureeDeVie    = batiment['periodeDeReference'] / construction_element['dureeVieTypique']
                                rp                  = max(1, refSurDureeDeVie)
                                iurc_production     = construction_element['impactUnitaireRechauffementClimatique']['production']
                                iurc_construction   = construction_element['impactUnitaireRechauffementClimatique']['construction']
                                iurc_exploitation   = construction_element['impactUnitaireRechauffementClimatique']['exploitation']
                                iurc_finDeVie       = construction_element['impactUnitaireRechauffementClimatique']['finDeVie']
                                impactExploitation += ((rp * iurc_exploitation)  + (rp-1) * (iurc_production + iurc_construction + iurc_finDeVie)) * zoneConstructionElement['quantite']
print(f'Impact Exploitation : {impactExploitation}')

impactFinDeVie = 0 
for batiment in batiments:
    if batiment['id'] == batimentId:
        for zoneId in batiment['zoneIds']:
            for zone in zones:
                if zone['id'] == zoneId:
                    for zoneConstructionElement in zone['constructionElements']:
                        for construction_element in construction_elements:
                            if construction_element['id'] == zoneConstructionElement['id']:
                                impactFinDeVie += construction_element['impactUnitaireRechauffementClimatique']['finDeVie'] * zoneConstructionElement['quantite']
print(f'Impact Fin De Vie : {impactFinDeVie}')

print(f'Impact Total : {impactProduction + impactConstruction + impactExploitation + impactFinDeVie}')