import json

class Batiment:
    """
    Une classe minimaliste pour représenter un bâtiment.

    ...
    
    Attributes
    ----------
    __id : int
        un index unique identifiant le bâtiment
    __nom : str
        le nom du bâtiment
    __surface : float
        la surface du bâtiment
    __zoneIds : List[int]
        la liste des identifiants uniques des zones constituant le bâtiment
    __usage : int
        identifiant de l'usage du bâtiment défini dans le fichier de données data/usages.json
    __periodeDeReference : int
        la période en années sur laquelle l'analyse de cycle de vie est effectuée

    Methods
    -------
    @staticmethod
    fromJson(data)
        Retourne une instance de Batiment avec les données de data.

    @staticmethod
    def serializeList(batiments):
        Serialize une liste d'instance de batiments et retourne le json.

    @staticmethod
    def serialize(batiment):
        Serialize une instance de batiment et retourne le json.

    def toDict(self):
        Retourne les paramètres du batiment dans un dictionnaire.
    """

    def __init__(self, id, nom,  surface, zoneIds, usage, periodeDeReference):
        """
        Parameters
        ----------
        id : int
            un index unique identifiant le bâtiment
        nom : str
            le nom du bâtiment
        surface : float
            la surface du bâtiment
        zoneIds : List[int]
            la liste des identifiants uniques des zones constituant le bâtiment
        usage : int
            identifiant de l'usage du bâtiment défini dans le fichier de données data/usages.json
        periodeDeReference : int
            la période en années sur laquelle l'analyse de cycle de vie est effectuée
        """
        self.__id                 = id
        self.__nom                = nom
        self.__surface            = surface
        self.__zoneIds            = zoneIds
        self.__usage              = usage
        self.__periodeDeReference = periodeDeReference

    @staticmethod
    def fromJson(data):
        """Retourne une instance de Batiment avec les données de data.

        TODO
        ----------
        Verification des données en entrée

        Parameters
        ----------
        data : dict
            Un dictionnaire contenant de manière exhaustive tous les paramètres d'un Batiment.

        Returns
        ----------
        Batiment
            Une instance de Batiment
        """
        return Batiment(data['id'], data['nom'], data['surface'], data['zoneIds'], data['usage'], data['periodeDeReference'])

    @staticmethod
    def serializeList(batiments):
        """Serialize une liste d'instance de batiments et retourne le json.

        TODO
        ----------
        Verification des données en entrée

        Parameters
        ----------
        batiments : List[Batiment]
            Une liste d'instance de Batiment à serialiser.

        Return
        ----------
        str
            La liste des Batiments sérialisée.
        """
        batimentsDictList = []
        for b in batiments:
            batimentsDictList.append(b.toDict())
        return json.dumps(batimentsDictList)

    @staticmethod
    def serialize(batiment):
        """Serialize une instance de batiment et retourne le json.

        TODO
        ----------
        Verification des données en entrée

        Parameters
        ----------
        batiment : Batiment
            Une instance de Batiment à serialiser.

        Return
        ----------
        str
            Le Batiment sérialisé.
        """
        return json.dumps(batiment.todict())

    def toDict(self):
        """Retourne les paramètres du batiment dans un dictionnaire.

        TODO
        ----------
        Verification des données en entrée

        Return
        ----------
        dict
            Les paramètres du batiment dans un dictionnaire.
        """
        return {
            "id":                 self.__id,
            "nom":                self.__nom,
            "surface":            self.__surface,
            "zoneIds":            self.__zoneIds,
            "usage":              self.__usage,
            "periodeDeReference": self.__periodeDeReference
        }

    def getId():
        return self.__id

    def setId(id):
        self.__id = id

    def getNom():
        return self.__nom

    def setNom(nom):
        self.__nom = nom

    def getSurface():
        return self.__surface

    def setSurface(surface):
        self.__surface = surface

    def getZoneIds():
        return self.__zoneIds

    def setZoneIds(zoneIds):
        self.__zoneIds = zoneIds

    def getUsage():
        return self.__usage

    def setUsage(usage):
        self.__usage = usage

    def getPeriodeDeReference():
        return self.__periodeDeReference

    def setPeriodeDeReference(periodeDeReference):
        self.__periodeDeReference = periodeDeReference