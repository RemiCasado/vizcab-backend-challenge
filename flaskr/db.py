"""Gestionnaire de la base de données / données brut json

Ce script instancie plusieurs fonctions de gestion de la base de données ou des données brut json.

Functions
-------
get_db()
    Retourne une interface sqlLite.

def close_db(e=None):
    Ferme l'interface sqlite de l'application.

def init_db():
    Execute le script schema.sql pour initialiser la base sqlLite. 

def init_db_command():
    Une commande flask pour executer la fonction init_db() en ligne de commande.

def get_batiments_data():
    Retourne une liste de Batiment initilisée par le jeu de données ./data/batiments.json.  
    @see ./flaskr/core/batiment.py

def init_app(app):
    Execute plusieurs scripts bd au demarrage de l'application.  
"""

import click
import json
import sqlite3
from flask import current_app, g
from flask.cli import with_appcontext
from . import core


def get_db():
    if 'db' not in g:
        g.db = sqlite3.connect(
            current_app.config['DATABASE'],
            detect_types=sqlite3.PARSE_DECLTYPES
        )
        g.db.row_factory = sqlite3.Row
    return g.db


def close_db(e=None):
    db = g.pop('db', None)

    if db is not None:
        db.close()

def init_db():
    db = get_db()

    with current_app.open_resource('schema.sql') as f:
        db.executescript(f.read().decode('utf8'))

@click.command('init-db')
@with_appcontext
def init_db_command():
    """Clear the existing data and create new tables."""
    init_db()
    click.echo('Initialized the database.')

def get_batiments_data():
    batiments = []
    with current_app.open_resource('data/batiments.json') as f:
        data = json.load(f)
        for d in data:
            batiments.append(core.batiment.Batiment.fromJson(d))
    return batiments

def init_app(app):
    # Ferme l'interface sqlLite à la fermeture du serveur.
    app.teardown_appcontext(close_db)
    # Ajoute l'initialisation des données dans une commande flask 
    app.cli.add_command(init_db_command)

