"""Blueprint de Batiments

Ce script instancie un blueprint pour gérer l'API concernant les batiments
"""

import json
from . import core
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, current_app
)
from werkzeug.exceptions import abort
from flaskr.db import get_batiments_data, get_db

bp = Blueprint('batiment', __name__)

@bp.route('/batiments/all', endpoint='all')
def all():
    """
    Retourne tous les batiments sous forme de json.
    ---
    responses:
        200:
            description: Une list de batiments.
            content:
                application/json:
                    schema:
                        type: array
                        items:
                            type: object
                            properties:
                                id:
                                    type: integer
                                    description: un index unique identifiant le bâtiment
                                    example: 4
                                nom:
                                    type: string
                                    description: le nom du bâtiment
                                    example: Auditorium de Lyon
                                surface:
                                    type: float
                                    description: la surface du bâtiment
                                    example: 137.2
                                zoneIds:
                                    type: array
                                    items:
                                        type: integer
                                    description: la liste des identifiants uniques des zones constituant le bâtiment
                                    example: [1,2]
                                usage:
                                    type: integer
                                    description: identifiant de l'usage du bâtiment
                                    example: 4
                                periodeDeReference:
                                    type: integer
                                    description: la période en années sur laquelle l'analyse de cycle de vie est effectuée
                                    example: 50
    """
    #TODO Utiliser la bdd plutôt que les sources json.
    batiments = get_batiments_data()
    return current_app.response_class(
        response=core.batiment.Batiment.serializeList(batiments),
        status=200,
        mimetype='application/json'
    )

@bp.route('/batiment/<int:id>/surface', endpoint='surfaceEndpoint', methods=['GET'])
def surfaceEndpoint(id):
    """
    Retourne la surface d'un batiment donné.
    ---
    parameters:
        - in: path
          name: id
          schema:
            type: integer
          required: true
          description: L'index d'un Batiment.
          exemple: 2
    responses:
        200:
            description: Un object contenant la surface du batiment.
            content:
                application/json:
                    schema:
                        type: object
                        properties:
                            surface:
                                type: integer
                                description: La surface du batiment.
                                example: 324
        404:
            description: Le batiment n'a pas été trouvé.
    """
    surface = get_db().execute(
        ' SELECT SUM(z.surface) '
        ' FROM zone z JOIN batiment_zone bz ON bz.zone_id = z.id'
        ' WHERE bz.batiment_id = ?'
        'LIMIT 1',
        (id,)
    ).fetchone()
    if surface[0] == None:
        abort(404, description="Resource not found")
    return current_app.response_class(
        response=json.dumps({'surface': surface[0]}),
        status=200,
        mimetype='application/json'
    )

@bp.route('/batiment/<int:id>/usage', endpoint='usageEndpoint', methods=['GET'])
def usageEndpoint(id):
    """
    Retourne l'usage majoritaire d'un batiment donné.
    ---
    parameters:
        - in: path
          name: id
          schema:
            type: integer
          required: true
          description: L'index d'un Batiment.
          exemple: 2
    responses:
        200:
            description: Un object contenant l'usage majoritaire du batiment.
            content:
                application/json:
                    schema:
                        type: object
                        properties:
                            usage:
                                type: string
                                description: L'usage majoritaire du batiment.
                                example: Salle de sport
        404:
            description: Le batiment n'a pas été trouvé.
    """
    usage = get_db().execute(
        ' SELECT u.nom, MAX(z.surface) '
        ' FROM zone z '
        ' LEFT JOIN batiment_zone bz ON bz.zone_id = z.id '
        ' LEFT JOIN usage u ON z.usage_id = u.id  '
        ' WHERE bz.batiment_id = ?'
        'LIMIT 1',
        (id,)
    ).fetchone()
    if usage[0] == None:
        abort(404, description="Resource not found")
    return current_app.response_class(
        response=json.dumps({'usage': usage[0]}),
        status=200,
        mimetype='application/json'
    )

def impactProduction(id):
    """Retourne l'impact de production d'un batiment donné.

    Parameters
    ----------
    id : int
        L'index d'un Batiment.

    Returns
    ----------
    float
        L'impact de production du batiment
    """
    impactProduction = get_db().execute(
        ' SELECT SUM(ce.iurc_production*zce.quantite) '
        ' FROM zone z '
        ' LEFT JOIN batiment_zone bz ON bz.zone_id = z.id '
        ' LEFT JOIN zone_construction_element zce ON zce.zone_id = z.id  '
        ' LEFT JOIN construction_element ce ON zce.construction_element_id = ce.id '
        ' WHERE bz.batiment_id = ? '
        'LIMIT 1',
        (id,)
    ).fetchone()
    return impactProduction[0]

@bp.route('/batiment/<int:id>/impact-production', endpoint='impactProductionEnpoint', methods=['GET'])
def impactProductionEnpoint(id):
    """
    Retourne l'impact de production d'un batiment donné.
    ---
    parameters:
        - in: path
          name: id
          schema:
            type: integer
          required: true
          description: L'index d'un Batiment.
          exemple: 2
    responses:
        200:
            description: Un object contenant l'impact de production du batiment.
            content:
                application/json:
                    schema:
                        type: object
                        properties:
                            impactProduction:
                                type: float
                                description: L'impact de production du batiment.
                                example: 12.1
        404:
            description: Le batiment n'a pas été trouvé.
    """
    ip = impactProduction(id)
    if ip == None:
        abort(404, description="Resource not found")
    return current_app.response_class(
        response=json.dumps({'impactProduction': ip}),
        status=200,
        mimetype='application/json'
    )

def impactConstruction(id):
    """Retourne l'impact de construction d'un batiment donné.

    Parameters
    ----------
    id : int
        L'index d'un Batiment.

    Returns
    ----------
    float
        L'impact de construction du batiment
    """
    impactConstruction = get_db().execute(
        ' SELECT SUM(ce.iurc_construction*zce.quantite) '
        ' FROM zone z '
        ' LEFT JOIN batiment_zone bz ON bz.zone_id = z.id '
        ' LEFT JOIN zone_construction_element zce ON zce.zone_id = z.id  '
        ' LEFT JOIN construction_element ce ON zce.construction_element_id = ce.id '
        ' WHERE bz.batiment_id = ? '
        'LIMIT 1',
        (id,)
    ).fetchone()
    return impactConstruction[0]

@bp.route('/batiment/<int:id>/impact-construction', endpoint='impactConstructionEndpoint', methods=['GET'])
def impactConstructionEndpoint(id):
    """
    Retourne l'impact de construction d'un batiment donné.
    ---
    parameters:
        - in: path
          name: id
          schema:
            type: integer
          required: true
          description: L'index d'un Batiment.
          exemple: 2
    responses:
        200:
            description: Un object contenant l'impact de construction du batiment.
            content:
                application/json:
                    schema:
                        type: object
                        properties:
                            impactConstruction:
                                type: float
                                description: L'impact de construction du batiment.
                                example: 12.1
        404:
            description: Le batiment n'a pas été trouvé.
    """
    ic = impactConstruction(id)
    if ic == None:
        abort(404, description="Resource not found")
    return current_app.response_class(
        response=json.dumps({'impactConstruction': ic}),
        status=200,
        mimetype='application/json'
    )

def impactExploitation(id):
    """Retourne l'impact d'exploitation d'un batiment donné.

    Parameters
    ----------
    id : int
        L'index d'un Batiment.

    Returns
    ----------
    float
        L'impact d'exploitation du batiment
    """
    # On cast b.periode_de_reference en float en le multipliant par 1.0 pour éviter que SQLite n'arrondisse le facteur Rp
    impactExploitation = get_db().execute(
        ' SELECT SUM( (MAX(1, b.periode_de_reference*1.0/ce.duree_vie_typique) * ce.iurc_exploitation + (MAX(1, b.periode_de_reference*1.0/ce.duree_vie_typique) - 1) * (ce.iurc_production + ce.iurc_construction + ce.iurc_fin_de_vie)) * zce.quantite) '
        ' FROM zone z '
        ' LEFT JOIN batiment_zone bz ON bz.zone_id = z.id '
        ' LEFT JOIN zone_construction_element zce ON zce.zone_id = z.id  '
        ' LEFT JOIN construction_element ce ON zce.construction_element_id = ce.id '
        ' LEFT JOIN batiment b ON b.id = bz.batiment_id'
        ' WHERE bz.batiment_id = ? '
        'LIMIT 1',
        (id,)
    ).fetchone()
    return impactExploitation[0]

@bp.route('/batiment/<int:id>/impact-exploitation', endpoint='impactExploitationEndpoint', methods=['GET'])
def impactExploitationEndpoint(id):
    """
    Retourne l'impact d'exploitation d'un batiment donné.
    ---
    parameters:
        - in: path
          name: id
          schema:
            type: integer
          required: true
          description: L'index d'un Batiment.
          exemple: 2
    responses:
        200:
            description: Un object contenant l'impact d'exploitation du batiment.
            content:
                application/json:
                    schema:
                        type: object
                        properties:
                            impactExploitation:
                                type: float
                                description: L'impact d'exploitation du batiment.
                                example: 12.1
        404:
            description: Le batiment n'a pas été trouvé.
    """
    ie = impactExploitation(id)
    if ie == None:
        abort(404, description="Resource not found")
    return current_app.response_class(
        response=json.dumps({'impactExploitation': ie}),
        status=200,
        mimetype='application/json'
    )

def impactFinDeVie(id):
    """Retourne l'impact de fin de vie d'un batiment donné.

    Parameters
    ----------
    id : int
        L'index d'un Batiment.

    Returns
    ----------
    float
        L'impact de fin de vie du batiment
    """
    impactFinDeVie = get_db().execute(
        ' SELECT SUM(ce.iurc_fin_de_vie*zce.quantite) '
        ' FROM zone z '
        ' LEFT JOIN batiment_zone bz ON bz.zone_id = z.id '
        ' LEFT JOIN zone_construction_element zce ON zce.zone_id = z.id  '
        ' LEFT JOIN construction_element ce ON zce.construction_element_id = ce.id '
        ' WHERE bz.batiment_id = ? '
        'LIMIT 1',
        (id,)
    ).fetchone()
    return impactFinDeVie[0]

@bp.route('/batiment/<int:id>/impact-fin-de-vie', endpoint='impactFinDeVieEndpoint', methods=['GET'])
def impactFinDeVieEndpoint(id):
    """
    Retourne l'impact de fin de vie d'un batiment donné.
    ---
    parameters:
        - in: path
          name: id
          schema:
            type: integer
          required: true
          description: L'index d'un Batiment.
          exemple: 2
    responses:
        200:
            description: Un object contenant l'impact de fin de vie du batiment.
            content:
                application/json:
                    schema:
                        type: object
                        properties:
                            impactFinDeVie:
                                type: float
                                description: L'impact de fin de vie du batiment.
                                example: 12.1
        404:
            description: Le batiment n'a pas été trouvé.
    """
    ifdv = impactFinDeVie(id)
    if ifdv == None:
        abort(404, description="Resource not found")
    return current_app.response_class(
        response=json.dumps({'impactFinDeVie': ifdv}),
        status=200,
        mimetype='application/json'
    )

@bp.route('/batiment/<int:id>/impact-total', endpoint='impactTotalEndpoint', methods=['GET'])
def impactTotalEndpoint(id):
    """
    Retourne l'impact total d'un batiment donné.
    ---
    parameters:
        - in: path
          name: id
          schema:
            type: integer
          required: true
          description: L'index d'un Batiment.
          exemple: 2
    responses:
        200:
            description: Un object contenant l'impact total du batiment.
            content:
                application/json:
                    schema:
                        type: object
                        properties:
                            impactProduction:
                                type: float
                                description: L'impact de production du batiment.
                                example: 12.1
                            impactConstruction:
                                type: float
                                description: L'impact de construction du batiment.
                                example: 12.1
                            impactExploitation:
                                type: float
                                description: L'impact d'exploitation' du batiment.
                                example: 12.1
                            impactFinDeVie:
                                type: float
                                description: L'impact de fin de vie du batiment.
                                example: 12.1
                            impactTotal:
                                type: float
                                description: L'impact total du batiment.
                                example: 12.1
        404:
            description: Le batiment n'a pas été trouvé.
    """
    ip          = impactProduction(id)
    if ip == None:
        abort(404, description="Resource not found")
    ic          = impactConstruction(id)
    ie          = impactExploitation(id)
    ifdv        = impactFinDeVie(id)
    impactTotal = ip + ic + ie + ifdv

    return current_app.response_class(
        response=json.dumps({
            'impactProduction':   ip,
            'impactConstruction': ic,
            'impactExploitation': ie,
            'impactFinDeVie':     ifdv,
            'impactTotal':        impactTotal
        }),
        status=200,
        mimetype='application/json'
    )