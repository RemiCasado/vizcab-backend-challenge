/*
Schema de la base avec les données initiales
*/

-- Table et entrée de Usage
CREATE TABLE usage (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    nom TEXT NOT NULL
);

INSERT INTO usage (id, nom) VALUES ('1', 'Logement collectif');
INSERT INTO usage (id, nom) VALUES ('2', 'Bureaux');
INSERT INTO usage (id, nom) VALUES ('3', 'Etablissement sportif municipal ou privé');
INSERT INTO usage (id, nom) VALUES ('4', 'Etablissement accueil petite enfance');
INSERT INTO usage (id, nom) VALUES ('5', 'Restauration commerciale en continu');

-- Table et entrée de Batiment
-- @see batiment_zone
CREATE TABLE batiment (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nom TEXT NOT NULL,
  surface FLOAT,
  usage INTEGER,
  periode_de_reference INTEGER NOT NULL
);

INSERT INTO batiment (id, nom,periode_de_reference) VALUES ('0', 'Résidence Les Orties', '50');
INSERT INTO batiment (id, nom,periode_de_reference) VALUES ('1', 'Siège social Entreprise D', '30');
INSERT INTO batiment (id, nom,periode_de_reference) VALUES ('2', 'Centre sportif', '50');

-- Table et entrée de Construction Element
CREATE TABLE construction_element (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nom TEXT NOT NULL,
  unite TEXT NOT NULL,
  iurc_production FLOAT NOT NULL,
  iurc_construction FLOAT NOT NULL,
  iurc_exploitation FLOAT NOT NULL,
  iurc_fin_de_vie FLOAT NOT NULL,
  duree_vie_typique INTEGER NOT NULL
);

INSERT INTO construction_element (id, nom, unite, iurc_production, iurc_construction, iurc_exploitation, iurc_fin_de_vie, duree_vie_typique) 
       VALUES ('10632', "Isolant en polyuréthane", 'm²', '17.5', '2.1', '0', '0.3', '50');
INSERT INTO construction_element (id, nom, unite, iurc_production, iurc_construction, iurc_exploitation, iurc_fin_de_vie, duree_vie_typique) 
       VALUES ('12454', "Dalle pleine en béton d'épaisseur 0.20 m", 'm²', '37.7', '15.8', '-2', '0.6', '100');
INSERT INTO construction_element (id, nom, unite, iurc_production, iurc_construction, iurc_exploitation, iurc_fin_de_vie, duree_vie_typique) 
       VALUES ('6396', "Mortiers de ragréage muraux", 'm²', '2.5', '0', '0', '0.8', '30');
INSERT INTO construction_element (id, nom, unite, iurc_production, iurc_construction, iurc_exploitation, iurc_fin_de_vie, duree_vie_typique) 
       VALUES ('13428', "Plafond suspendu en plaque de plâtre", 'm²', '10.8', '3.4', '0', '0.1', '50');
INSERT INTO construction_element (id, nom, unite, iurc_production, iurc_construction, iurc_exploitation, iurc_fin_de_vie, duree_vie_typique) 
       VALUES ('18614', "Refend, Voile ou Mur intérieur en béton armé d'épaisseur 0.20 m", 'm²', '37.7', '7.6', '-5.9', '1.6', '100');
INSERT INTO construction_element (id, nom, unite, iurc_production, iurc_construction, iurc_exploitation, iurc_fin_de_vie, duree_vie_typique) 
       VALUES ('16233', "Escalier droit en béton", 'm²', '303', '48.3', '-10.6', '-6.4', '100');
INSERT INTO construction_element (id, nom, unite, iurc_production, iurc_construction, iurc_exploitation, iurc_fin_de_vie, duree_vie_typique) 
       VALUES ('8101', "Acier de ferraillage", 'm²', '3.1', '0.4', '0', '0.2', '100');
INSERT INTO construction_element (id, nom, unite, iurc_production, iurc_construction, iurc_exploitation, iurc_fin_de_vie, duree_vie_typique) 
       VALUES ('8779', "Solins et bandes de rives en aluminium", 'm²', '17.8', '3.3', '0', '0.2', '20');
INSERT INTO construction_element (id, nom, unite, iurc_production, iurc_construction, iurc_exploitation, iurc_fin_de_vie, duree_vie_typique) 
       VALUES ('7993', "Géotextile en polypropylène", 'm²', '1.2', '0.1', '0', '0.1', '50');
INSERT INTO construction_element (id, nom, unite, iurc_production, iurc_construction, iurc_exploitation, iurc_fin_de_vie, duree_vie_typique) 
       VALUES ('9054', "Ossature métallique acier", 'm²', '2.7', '0.3', '0', '0.01', '100');
INSERT INTO construction_element (id, nom, unite, iurc_production, iurc_construction, iurc_exploitation, iurc_fin_de_vie, duree_vie_typique) 
       VALUES ('10806', "Moquette touffetée", 'm²', '16.4', '1.2', '2.4', '0.3', '10');
INSERT INTO construction_element (id, nom, unite, iurc_production, iurc_construction, iurc_exploitation, iurc_fin_de_vie, duree_vie_typique) 
       VALUES ('25781', "Revêtement de sol PVC sur liège", 'm²', '7.8', '1.2', '3.6', '0.4', '25');

-- Table et entrée de Zone
-- @see zone_construction_element
-- @see usage
-- @see batiment_zone
CREATE TABLE zone (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    nom TEXT NOT NULL,
    surface FLOAT NOT NULL,
    usage_id INTEGER NOT NULL,
    FOREIGN KEY (usage_id) REFERENCES usage (id)
);

INSERT INTO zone (id, nom, surface, usage_id) VALUES ('0', "Les Orties - Escalier A", '411', '1');
INSERT INTO zone (id, nom, surface, usage_id) VALUES ('1', "Les Orties - Escalier B", '189', '1');
INSERT INTO zone (id, nom, surface, usage_id) VALUES ('2', "Les Orties - Crêche", '170', '4');
INSERT INTO zone (id, nom, surface, usage_id) VALUES ('3', "Restaurant Du Pain Aux Orties", '215', '5');
INSERT INTO zone (id, nom, surface, usage_id) VALUES ('4', "Entreprise D", '1243', '2');
INSERT INTO zone (id, nom, surface, usage_id) VALUES ('5', "Espace de coworking - DLab", '812', '2');
INSERT INTO zone (id, nom, surface, usage_id) VALUES ('6', "Sport & Co", '620', '3');

-- Table et entrée de zone_construction_element
-- @see zone
-- @see construction_element
CREATE TABLE zone_construction_element (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    zone_id INTEGER NOT NULL,
    construction_element_id INTEGER NOT NULL,
    quantite FLOAT NOT NULL,
    FOREIGN KEY (zone_id) REFERENCES zone (id),
    FOREIGN KEY (construction_element_id) REFERENCES construction_element (id)
);

INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('0', '10632', '175.6');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('0', '12454', '175.6');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('0', '6396', '283.6');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('0', '13428', '152.1');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('0', '18614', '234.3');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('0', '16233', '6.1');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('0', '8101', '370');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('0', '7993', '49.3');

INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('1', '10632', '79.3');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('1', '12454', '85.2');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('1', '6396', '130.7');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('1', '13428', '59.4');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('1', '18614', '106.9');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('1', '16233', '5.9');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('1', '8101', '292.3');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('1', '7993', '24');

INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('2', '10632', '74.8');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('2', '12454', '83.6');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('2', '6396', '114.6');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('2', '13428', '52.9');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('2', '18614', '87.8');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('2', '16233', '1');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('2', '10806', '113.1');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('2', '25781', '45.1');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('2', '8101', '206.3');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('2', '7993', '14.7');

INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('3', '10632', '67.8');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('3', '12454', '103.4');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('3', '6396', '113.2');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('3', '13428', '78');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('3', '18614', '70.9');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('3', '16233', '1.5');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('3', '8779', '23.7');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('3', '10806', '115.5');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('3', '25781', '79.4');

INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('4', '10632', '474.6');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('4', '12454', '599.7');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('4', '6396', '566');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('4', '13428', '468.1');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('4', '18614', '480.9');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('4', '16233', '15.2');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('4', '8779', '137.1');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('4', '10806', '942.6');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('4', '25781', '368.4');

INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('5', '10632', '304.1');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('5', '12454', '325.2');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('5', '6396', '199.6');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('5', '13428', '324.3');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('5', '18614', '280.7');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('5', '16233', '5.1');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('5', '10806', '760.4');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('5', '25781', '181');

INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('6', '10632', '272.2');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('6', '12454', '301.5');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('6', '6396', '219.4');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('6', '13428', '668.1');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('6', '18614', '230.9');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('6', '16233', '23.4');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('6', '8779', '92.4');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('6', '10806', '62');
INSERT INTO zone_construction_element (zone_id, construction_element_id, quantite) VALUES ('6', '25781', '608.3');

-- Table et entrée de batiment_zone
-- @see zone
-- @see batiment
CREATE TABLE batiment_zone (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    batiment_id INTEGER NOT NULL,
    zone_id INTEGER NOT NULL,
    FOREIGN KEY (zone_id) REFERENCES zone (id),
    FOREIGN KEY (batiment_id) REFERENCES batiment (id)
);

INSERT INTO batiment_zone (batiment_id, zone_id) VALUES ('0', '0');
INSERT INTO batiment_zone (batiment_id, zone_id) VALUES ('0', '1');
INSERT INTO batiment_zone (batiment_id, zone_id) VALUES ('0', '2');
INSERT INTO batiment_zone (batiment_id, zone_id) VALUES ('0', '3');
INSERT INTO batiment_zone (batiment_id, zone_id) VALUES ('1', '4');
INSERT INTO batiment_zone (batiment_id, zone_id) VALUES ('1', '5');
INSERT INTO batiment_zone (batiment_id, zone_id) VALUES ('2', '6');
