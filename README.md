# Vizcab Backend Python

## Installation

Le projet a été dev avec python 3.8.

(Windows) Installation de virtualenv & flask
```
py -m pip install virtualenv
py -3 -m venv venv
venv\Scripts\activate
pip install -r requirements.txt
```

## Initialisation de la base de données

Le projet tourne avec une base SQLite. 

Il faut l'initialier une première fois avec la commande *init-db*.

(Windows)
```
venv\Scripts\activate
$env:FLASK_ENV = "development"
$env:FLASK_APP = "flaskr"
flask init-db
```

La fonctionnalité qui liste tous les batiments, codée au début du projet, utilise les données brutes.

Pour cela, copier le dossier *data* de https://github.com/Naybnet/vizcab-backend-challenge dans le dossier *flaskr/*


## Démarrage du serveur

(Windows)
```
venv\Scripts\activate
$env:FLASK_ENV = "development"
$env:FLASK_APP = "flaskr"
flask run
```

Le serveur est accessible à l'adresse http://127.0.0.1:5000/

Les infos sur un batiment sont accessibles via les routes :

|                     |                                    | 
| ------------------- | ---------------------------------- | 
| usage               | /batiment/{id}/usage               |
| surface             | /batiment/{id}/surface             |
| impact production   | /batiment/{id}/impact-production   |
| impact construction | /batiment/{id}/impact-construction |
| impact exploitation | /batiment/{id}/impact-exploitation |
| impact fin de vie   | /batiment/{id}/impact-fin-de-vie   |
| impact total        | /batiment/{id}/impact-total        |
| tous les batiments  | /batiments/all                     |

## Documentation

Les endpoints de l'API sont listés à l'adresse http://127.0.0.1:5000/spec

J'utilise la lib https://pypi.org/project/flask-swagger/ pour documenter les endpoints.

Ça risque de piquer les yeux, on n'est pas sur l'interface du siècle !

## Docker

Le dockerfile est fortement inspiré de https://runnable.com/docker/python/dockerize-your-flask-application.

```
docker build -t flask-tutorial:latest .
docker run -d -p 5000:5000 flask-tutorial
```